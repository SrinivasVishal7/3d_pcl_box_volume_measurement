// ========================================================================================
//  ApproxMVBB
//  Copyright (C) 2014 by Gabriel Nützi <nuetzig (at) imes (d0t) mavt (d0t) ethz
//  (døt) ch>
//
//  This Source Code Form is subject to the terms of the Mozilla Public
//  License, v. 2.0. If a copy of the MPL was not distributed with this
//  file, You can obtain one at http://mozilla.org/MPL/2.0/.
// ========================================================================================

#include <iostream>
#include "ApproxMVBB/ComputeApproxMVBB.hpp"
#include <fstream>
#include <iostream>
#include <vector>         
#include "ApproxMVBB/Config/Config.hpp"

#include <fstream>
#include <iostream>
#include <vector>
#include "ApproxMVBB/Config/Config.hpp"
#include ApproxMVBB_TypeDefs_INCLUDE_FILE
#include "ApproxMVBB/Common/CPUTimer.hpp"

ApproxMVBB_DEFINE_MATRIX_TYPES;
ApproxMVBB_DEFINE_POINTS_CONFIG_TYPES;

Vector3List getPointsFromFile3D(std::string);

int main(int , char** )
{
        auto point_1 = getPointsFromFile3D("./files/cam_1.txt");
	Matrix3Dyn points(3, point_1.size());
        for(unsigned int i = 0; i < point_1.size(); ++i)
        {
          points.col(i) = point_1[i];
        }
        ApproxMVBB::OOBB oobb = ApproxMVBB::approximateMVBB(points,0.001,500,5,0,5);
// ApproxMVBB::approximateMVBB(pts, epsilon, pointSamples, gridSize, mvbbDiamOptLoops, mvbbGridSearchOptLoops)


            std::cout << std::endl;
            std::cout << std::endl;
    std::cout << "Computed OOBB: " << std::endl
              << "---> lower point in OOBB coordinate system: " << oobb.m_minPoint.transpose() << std::endl
              << "---> upper point in OOBB coordinate system: " << oobb.m_maxPoint.transpose() << std::endl
              << "---> coordinate transformation A_IK matrix from OOBB coordinate system `K`  "
                 "to world coordinate system `I` "
              << std::endl
              << oobb.m_q_KI.matrix() << std::endl
              << "---> this is also the rotation matrix R_KI  which turns the "
                 "world coordinate system `I`  into the OOBB coordinate system `K` "
              << std::endl
              << std::endl;
    // To make all points inside the OOBB :
    ApproxMVBB::Matrix33 A_KI = oobb.m_q_KI.matrix().transpose();  // faster to store the transformation matrix first
    auto size                 = points.cols();
    for(unsigned int i = 0; i < size; ++i)
    {
        oobb.unite(A_KI * points.col(i));
    }

    oobb.expandToMinExtentAbsolute(0.1);

    std::cout << "OOBB with all point included: " << std::endl
              << "---> lower point in OOBB coordinate system: " << oobb.m_minPoint.transpose() << std::endl
              << "---> upper point in OOBB coordinate system: " << oobb.m_maxPoint.transpose() << std::endl;

    Vector3List pointsV = oobb.getCornerPoints();
    Matrix3Dyn pV(3, pointsV.size());

          for(unsigned int i = 0; i < 8; ++i)
            {
            std::cout << pointsV[i];
            std::cout << std::endl;
            std::cout << std::endl;
            }
    return 0;
}

Vector3List getPointsFromFile3D(std::string filePath)
{
    std::ifstream file;           // creates stream myFile
    file.open(filePath.c_str());  // opens .txt file

    if(!file.is_open())
    {  // check file is open, quit if not
        ApproxMVBB_ERRORMSG("Could not open file: " << filePath)
    }

    PREC a, b, c;
    Vector3List v;
    while(file.good())
    {
        file >> a >> b >> c;
        v.emplace_back(a, b, c);
    }
    file.close();
    return v;
}

